.SILENT: all
.PHONY: all

all:
	echo "Generate code.pdf..."
	cd cv; \
	pandoc						 	    		     \
		--from				   				markdown \
		--template		   cv.tex \
		--metadata-file   			   metadata.yaml \
		--pdf-engine		    			 xelatex \
		--highlight-style  kate \
		--out           	   				../cv.pdf \
	cv.md
	echo "Successfully generated at code.pdf!"

clean:
	rm -f code.pdf
