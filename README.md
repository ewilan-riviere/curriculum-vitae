# Curriculum Vitae

Generate Curriculum Vitae PDF version

```bash
make
```

- <https://github.com/JanHendrikDolling/latex-fontawesome5>
- <https://github.com/posquit0/Awesome-CV>
- <https://github.com/xdanaux/moderncv>
